import helper
from sqlalchemy.orm import sessionmaker
from models import Team, TeamAlias, Competition, CompetitionAlias, Season

engine = helper.getDatabaseEngine()
Session = sessionmaker(bind=engine)
session = Session()

teamNamesArray = [Team(name="Arsenal", teamsAlias=[TeamAlias(alias="ARSENAL"), TeamAlias(alias="GUNNERS")]),
                  Team(name="Aston Villa", teamsAlias=[TeamAlias(alias="ASTON VILLA"), TeamAlias(alias="VILLANS"), TeamAlias(alias="VILLA")]),
                  Team(name="Barnsley", teamsAlias=[TeamAlias(alias="BARNSLEY"), TeamAlias(alias="TARN")]),
                  Team(name="Birmingham City", teamsAlias=[TeamAlias(alias="BIRMINGHAM CITY")]),
                  Team(name="Blackburn Rovers", teamsAlias=[TeamAlias(alias="BLACKBURN ROVERS"), TeamAlias(alias="RIVERSIDERS")]),
                  Team(name="Blackpool", teamsAlias=[TeamAlias(alias="BLACKPOOL")]),
                  Team(name="Bolton Wanderers", teamsAlias=[TeamAlias(alias="BOLTON WANDERERS"), TeamAlias(alias="TROTTERS")]),
                  Team(name="AFC Bournemouth", teamsAlias=[TeamAlias(alias="AFC BOURNEMOUTH"), TeamAlias(alias="CHERRIES"), TeamAlias(alias="POPPIES"), TeamAlias(alias="BOURNEMOUTH")]),
                  Team(name="Bradford City", teamsAlias=[TeamAlias(alias="BRADFORD CITY"), TeamAlias(alias="BANTAMS")]),
                  Team(name="Brighton and Hove Albion", teamsAlias=[TeamAlias(alias="BRIGHTON AND HOVE ALBION"), TeamAlias(alias="SEAGULLS")]),
                  Team(name="Burnley", teamsAlias=[TeamAlias(alias="BURNLEY")]),
                  Team(name="Cardiff City", teamsAlias=[TeamAlias(alias="CARDIFF CITY"), TeamAlias(alias="BLUEBIRDS")]),
                  Team(name="Charlton Athletic", teamsAlias=[TeamAlias(alias="CHARLTON ATHLETIC"), TeamAlias(alias="ADDICKS")]),
                  Team(name="Chelsea", teamsAlias=[TeamAlias(alias="CHELSEA")]),
                  Team(name="Coventry City", teamsAlias=[TeamAlias(alias="COVENTRY CITY")]),
                  Team(name="Crystal Palace", teamsAlias=[TeamAlias(alias="CRYSTAL PALACE"), TeamAlias(alias="EAGLES")]),
                  Team(name="Derby County", teamsAlias=[TeamAlias(alias="DERBY COUNTY"), TeamAlias(alias="RAMS")]),
                  Team(name="Everton", teamsAlias=[TeamAlias(alias="EVERTON"), TeamAlias(alias="TOFFEES")]),
                  Team(name="Fulham", teamsAlias=[TeamAlias(alias="FULHAM"), TeamAlias(alias="COTTAGERS")]),
                  Team(name="Huddersfield Town", teamsAlias=[TeamAlias(alias="HUDDERSFIELD TOWN"), TeamAlias(alias="TERRIERS")]),
                  Team(name="Hull City", teamsAlias=[TeamAlias(alias="HULL CITY")]),
                  Team(name="Ipswich Town", teamsAlias=[TeamAlias(alias="IPSWICH TOWN")]),
                  Team(name="Leeds United", teamsAlias=[TeamAlias(alias="LEEDS UNITED")]),
                  Team(name="Leicester City", teamsAlias=[TeamAlias(alias="LEICESTER CITY"), TeamAlias(alias="FOXES")]),
                  Team(name="Liverpool", teamsAlias=[TeamAlias(alias="LIVERPOOL")]),
                  Team(name="Manchester City", teamsAlias=[TeamAlias(alias="MANCHESTER CITY"), TeamAlias(alias="CITIZENS"), TeamAlias(alias="CITYZENS"), TeamAlias(alias="MAN CITY"), TeamAlias(alias="MANCITY")]),
                  Team(name="Manchester United", teamsAlias=[TeamAlias(alias="MANCHESTER UNITED"), TeamAlias(alias="RED DEVILS"), TeamAlias(alias="REDDEVILS")]),
                  Team(name="Middlesbrough", teamsAlias=[TeamAlias(alias="MIDDLESBROUGH"), TeamAlias(alias="BORO"), TeamAlias(alias="SMOGGIES")]),
                  Team(name="Newcastle United", teamsAlias=[TeamAlias(alias="NEWCASTLE UNITED")]),
                  Team(name="Norwich City", teamsAlias=[TeamAlias(alias="NORWICH CITY"), TeamAlias(alias="CANARIES")]),
                  Team(name="Nottingham Forest", teamsAlias=[TeamAlias(alias="NOTTINGHAM FOREST"), TeamAlias(alias="TRICKY TREES")]),
                  Team(name="Oldham Athletic", teamsAlias=[TeamAlias(alias="OLDHAM ATHLETIC"), TeamAlias(alias="LATICS")]),
                  Team(name="Portsmouth", teamsAlias=[TeamAlias(alias="PORTSMOUTH"), TeamAlias(alias="POMPEY")]),
                  Team(name="Queens Park Rangers", teamsAlias=[TeamAlias(alias="QUEENS PARK RANGERS"), TeamAlias(alias="QPR")]),
                  Team(name="Reading", teamsAlias=[TeamAlias(alias="READING"), TeamAlias(alias="ROYALS"), TeamAlias(alias="BISCUITMEN")]),
                  Team(name="Sheffield United", teamsAlias=[TeamAlias(alias="SHEFFIELD UNITED"), TeamAlias(alias="BLADES")]),
                  Team(name="Sheffield Wednesday", teamsAlias=[TeamAlias(alias="SHEFFIELD WEDNESDAY"), TeamAlias(alias="OWLS"), TeamAlias(alias="WEDNESDAY")]),
                  Team(name="Southampton", teamsAlias=[TeamAlias(alias="SOUTHAMPTON"), TeamAlias(alias="SAINTS")]),
                  Team(name="Stoke City", teamsAlias=[TeamAlias(alias="STOKE CITY"), TeamAlias(alias="POTTERS")]),
                  Team(name="Sunderland", teamsAlias=[TeamAlias(alias="SUNDERLAND"), TeamAlias(alias="BLACK CATS"), TeamAlias(alias="LADS")]),
                  Team(name="Swansea City", teamsAlias=[TeamAlias(alias="SWANSEA CITY"), TeamAlias(alias="SWANS")]),
                  Team(name="Swindon Town", teamsAlias=[TeamAlias(alias="SWINDON TOWN")]),
                  Team(name="Tottenham Hotspur", teamsAlias=[TeamAlias(alias="TOTTENHAM HOTSPUR"), TeamAlias(alias="SPURS"), TeamAlias(alias="TOTTENHAM")]),
                  Team(name="Watford", teamsAlias=[TeamAlias(alias="WATFORD"), TeamAlias(alias="HORNETS")]),
                  Team(name="West Bromwich Albion", teamsAlias=[TeamAlias(alias="WEST BROMWICH ALBION"), TeamAlias(alias="BAGGIES"), TeamAlias(alias="THE THROSTLES"), TeamAlias(alias="THROSTLES"), TeamAlias(alias="WEST BROM"), TeamAlias(alias="WESTBROM")]),
                  Team(name="West Ham United", teamsAlias=[TeamAlias(alias="WEST HAM UNITED"), TeamAlias(alias="HAMMERS"), TeamAlias(alias="WEST HAM"), TeamAlias(alias="WESTHAM")]),
                  Team(name="Wigan Athletic", teamsAlias=[TeamAlias(alias="WIGAN ATHLETIC"), TeamAlias(alias="WIGAN")]),
                  Team(name="Wimbledon FC", teamsAlias=[TeamAlias(alias="WIMBLEDON"), TeamAlias(alias="WIMBLEDON FC"), TeamAlias(alias="WIMBLEDONFC")]),
                  Team(name="Wolverhampton Wanderers", teamsAlias=[TeamAlias(alias="WOLVERHAMPTON WANDERERS"), TeamAlias(alias="WOLVES")])]

competitionNamesArray = [Competition(name="English Premier League", competitionsAlias=[CompetitionAlias(alias="PREMIER LEAGUE"), CompetitionAlias(alias="PREMIERLEAGUE"), CompetitionAlias(alias="EPL")])]
eplCompetition = session.query(Competition).filter(Competition.name == "English Premier League").first()
seasonDetailsArray = [Season(name='1992-93', seasonNumber=1, startDate='1992-08-15', endDate='1993-05-11', url='https://www.skysports.com/premier-league-results/1992-93', competition_id = eplCompetition.id),
                      Season(name='1993-94', seasonNumber=2, startDate='1993-08-14', endDate='1994-05-08', url='https://www.skysports.com/premier-league-results/1993-94', competition_id = eplCompetition.id),
                      Season(name='1994-95', seasonNumber=3, startDate='1994-08-20', endDate='1995-05-14', url='https://www.skysports.com/premier-league-results/1994-95', competition_id = eplCompetition.id),
                      Season(name='1995-96', seasonNumber=4, startDate='1995-08-19', endDate='1996-05-05', url='https://www.skysports.com/premier-league-results/1995-96', competition_id = eplCompetition.id),
                      Season(name='1996-97', seasonNumber=5, startDate='1996-08-17', endDate='1997-05-11', url='https://www.skysports.com/premier-league-results/1996-97', competition_id = eplCompetition.id),
                      Season(name='1997-98', seasonNumber=6, startDate='1997-08-09', endDate='1998-05-10', url='https://www.skysports.com/premier-league-results/1997-98', competition_id = eplCompetition.id),
                      Season(name='1998-99', seasonNumber=7, startDate='1998-08-15', endDate='1999-05-16', url='https://www.skysports.com/premier-league-results/1998-99', competition_id = eplCompetition.id),
                      Season(name='1999-00', seasonNumber=8, startDate='1999-08-07', endDate='2000-05-14', url='https://www.skysports.com/premier-league-results/1999-00', competition_id = eplCompetition.id),
                      Season(name='2000-01', seasonNumber=9, startDate='2000-08-19', endDate='2001-05-19', url='https://www.skysports.com/premier-league-results/2000-01', competition_id = eplCompetition.id),
                      Season(name='2001-02', seasonNumber=10, startDate='2001-08-18', endDate='2002-05-11', url='https://www.skysports.com/premier-league-results/2001-02', competition_id = eplCompetition.id),
                      Season(name='2002-03', seasonNumber=11, startDate='2002-08-17', endDate='2003-05-11', url='https://www.skysports.com/premier-league-results/2002-03', competition_id = eplCompetition.id),
                      Season(name='2003-04', seasonNumber=12, startDate='2003-08-16', endDate='2004-05-15', url='https://www.skysports.com/premier-league-results/2003-04', competition_id = eplCompetition.id),
                      Season(name='2004-05', seasonNumber=13, startDate='2004-08-14', endDate='2005-05-15', url='https://www.skysports.com/premier-league-results/2004-05', competition_id = eplCompetition.id),
                      Season(name='2005-06', seasonNumber=14, startDate='2005-08-13', endDate='2006-05-07', url='https://www.skysports.com/premier-league-results/2005-06', competition_id = eplCompetition.id),
                      Season(name='2006-07', seasonNumber=15, startDate='2006-08-19', endDate='2007-05-13', url='https://www.skysports.com/premier-league-results/2006-07', competition_id = eplCompetition.id),
                      Season(name='2007-08', seasonNumber=16, startDate='2007-08-11', endDate='2008-05-11', url='https://www.skysports.com/premier-league-results/2007-08', competition_id = eplCompetition.id),
                      Season(name='2008-09', seasonNumber=17, startDate='2008-08-16', endDate='2009-05-24', url='https://www.skysports.com/premier-league-results/2008-09', competition_id = eplCompetition.id),
                      Season(name='2009-10', seasonNumber=18, startDate='2009-08-15', endDate='2010-05-09', url='https://www.skysports.com/premier-league-results/2009-10', competition_id = eplCompetition.id),
                      Season(name='2010-11', seasonNumber=19, startDate='2010-08-14', endDate='2011-05-22', url='https://www.skysports.com/premier-league-results/2010-11', competition_id = eplCompetition.id),
                      Season(name='2011-12', seasonNumber=20, startDate='2011-08-13', endDate='2012-05-13', url='https://www.skysports.com/premier-league-results/2011-12', competition_id = eplCompetition.id),
                      Season(name='2012-13', seasonNumber=21, startDate='2012-08-18', endDate='2013-05-19', url='https://www.skysports.com/premier-league-results/2012-13', competition_id = eplCompetition.id),
                      Season(name='2013-14', seasonNumber=22, startDate='2013-08-17', endDate='2014-05-11', url='https://www.skysports.com/premier-league-results/2013-14', competition_id = eplCompetition.id),
                      Season(name='2014-15', seasonNumber=23, startDate='2014-08-16', endDate='2015-05-24', url='https://www.skysports.com/premier-league-results/2014-15', competition_id = eplCompetition.id),
                      Season(name='2015-16', seasonNumber=24, startDate='2015-08-08', endDate='2016-05-17', url='https://www.skysports.com/premier-league-results/2015-16', competition_id = eplCompetition.id),
                      Season(name='2016-17', seasonNumber=25, startDate='2016-08-13', endDate='2017-05-21', url='https://www.skysports.com/premier-league-results/2016-17', competition_id = eplCompetition.id),
                      Season(name='2017-18', seasonNumber=26, startDate='2017-08-11', endDate='2018-05-13', url='https://www.skysports.com/premier-league-results/2017-18', competition_id = eplCompetition.id),
                      Season(name='2018-19', seasonNumber=27, startDate='2018-08-10', endDate='2019-05-12', url='https://www.skysports.com/premier-league-results/2018-19', competition_id = eplCompetition.id),
                      Season(name='2019-20', seasonNumber=28, startDate='2019-08-09', endDate='2020-05-17', url='https://www.skysports.com/premier-league-results/2019-20', competition_id = eplCompetition.id)]

session.add_all(teamNamesArray)
session.add_all(competitionNamesArray)
session.add_all(seasonDetailsArray)

session.commit()
