import configparser
from sqlalchemy import create_engine

def getDatabaseEngine():
    config = configparser.ConfigParser()
    config.read('config.ini')
    host = config["DATABASE"]["host"]
    username = config["DATABASE"]["user"]
    passwd = config["DATABASE"]["passwd"]
    database = config["DATABASE"]["database"]

    engine = create_engine("mysql://" + username + ":" + passwd + "@" + host + "/" + database, echo=True)
    return engine

def get_month_number(month):
    if month == "Jan" or month == "January":
        return 1
    if month == "Feb" or month == "February":
        return 2
    if month == "Mar" or month == "March":
        return 3
    if month == "Apr" or month == "April":
        return 4
    if month == "May" or month == "May":
        return 5
    if month == "Jun" or month == "June":
        return 6
    if month == "Jul" or month == "July":
        return 7
    if month == "Aug" or month == "August":
        return 8
    if month == "Sep" or month == "September":
        return 9
    if month == "Oct" or month == "October":
        return 10
    if month == "Nov" or month == "November":
        return 11
    if month == "Dec" or month == "December":
        return 12

    return "Error in input - month does not exist"
