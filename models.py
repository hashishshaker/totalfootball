import helper
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

engine = helper.getDatabaseEngine()
Base = declarative_base()

class Team(Base):
    __tablename__ = "teams"
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    name = Column(String(255), nullable=False, unique=True)

    teamsAlias = relationship("TeamAlias", back_populates="team")

    def __repr__(self):
        return '<Team %r>' % self.name

class TeamAlias(Base):
    __tablename__ = "teamsAlias"
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    alias = Column(String(255), nullable=False, unique=True)
    team_id = Column(Integer, ForeignKey('teams.id'), nullable=False)

    team = relationship("Team", back_populates="teamsAlias")

    def __repr__(self):
        return "<TeamAlias(alias='%s')>" % self.alias

class Competition(Base):
    __tablename__ = "competitions"
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    name = Column(String(255), nullable=False, unique=True)

    competitionsAlias = relationship("CompetitionAlias", back_populates="competition")
    seasons = relationship("Season", backref="competition")

    def __repr__(self):
        return '<Competition %r>' % self.name

class CompetitionAlias(Base):
    __tablename__ = "competitionsAlias"
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    alias = Column(String(255), nullable=False, unique=True)
    competition_id = Column(Integer, ForeignKey('competitions.id'), nullable=False)

    competition = relationship("Competition", back_populates="competitionsAlias")

    def __repr__(self):
        return "<CompetitionAlias(alias='%s')>" % self.alias

class Season(Base):
    __tablename__ = "seasons"
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    name = Column(String(255), nullable=False)
    seasonNumber = Column(Integer, nullable=False)
    startDate = Column(Date, nullable=False)
    endDate = Column(Date, nullable=False)
    url = Column(String(255), nullable=False)
    competition_id = Column(Integer, ForeignKey('competitions.id'), nullable=False)

    def __repr__(self):
        return '<Season %r>' % self.name

class Match(Base):
    __tablename__ = "matches"
    id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    date = Column(Date, nullable=False)
    team1id = Column(Integer, ForeignKey('teams.id'), nullable=False)
    team2id = Column(Integer, ForeignKey('teams.id'), nullable=False)
    team1goals = Column(Integer, nullable=False)
    team2goals = Column(Integer, nullable=False)
    url = Column(String(255), nullable=False)

    team1 = relationship("Team", foreign_keys=[team1id])
    team2 = relationship("Team", foreign_keys=[team2id])

Base.metadata.create_all(engine)
