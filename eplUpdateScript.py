import mainfunctions, helper
from models import Team, TeamAlias, Match, Season
from sqlalchemy.orm import sessionmaker

engine = helper.getDatabaseEngine()
Session = sessionmaker(bind=engine)
session = Session()

currentSeason = mainfunctions.getCurrentSeason()
seasonurl = session.query(Season.url).filter(Season.name == currentSeason).first()[0]
matches = mainfunctions.getMatchesDetailsFromSeasonLink(seasonurl)
matchesToAddToDB = []

for match in matches:
    team1id = (session.query(Team).join(TeamAlias).filter(TeamAlias.alias == match["team1"].upper()).first()).id
    team2id = (session.query(Team).join(TeamAlias).filter(TeamAlias.alias == match["team2"].upper()).first()).id
    matchdate = match["date"]
    team1goals = match["team1goals"]
    team2goals = match["team2goals"]
    url = match["url"]
    result = (session.query(Match).filter(Match.date == matchdate).filter(Match.team1id == team1id).filter(Match.team2id == team2id)).all()

    if len(result) == 0:
        matchesToAddToDB.append(Match(date=matchdate, team1id=team1id, team2id=team2id, team1goals=team1goals, team2goals=team2goals, url=url))

session.add_all(matchesToAddToDB)
session.commit()
