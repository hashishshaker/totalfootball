import datetime, helper
import requests
from bs4 import BeautifulSoup
from sqlalchemy.orm import sessionmaker
from models import Season

def getMatchesDetailsFromSeasonLink(url):
    output = []
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    my_url = requests.get(url, headers=headers)

    soup = BeautifulSoup(my_url.text, 'html.parser')

    content1 = soup.find(attrs={"class": "fixres__body"})

    if content1.script is not None:
        content2 = BeautifulSoup(content1.script.extract().text, "lxml")
        links = content1.find_all("a") + content2.find_all("a")
    else:
        links = content1.find_all("a")

    for link in links:
        matchurl = requests.get(link.get("href"), headers=headers)
        print("started with " + link.get("href"))
        linkSoup = BeautifulSoup(matchurl.text, "lxml")

        teams = linkSoup.find_all(attrs={"class": "match-head__team-name"})
        goals = linkSoup.find_all(attrs={"class": "match-head__score"})

        if len(teams) > 0:
            matchDateParts = linkSoup.find(attrs={"name": "description"}).get("content").split("(")[1].split(")")[
                0].split(" ")
            matchDate = datetime.datetime(int(matchDateParts[2]),
                                          int(helper.get_month_number(matchDateParts[1].strip())),
                                          int(matchDateParts[0]))

            toBeAppended = {}
            toBeAppended["date"] = matchDate
            toBeAppended["team1"] = teams[0].find("abbr").get("title").strip()
            toBeAppended["team2"] = teams[1].find("abbr").get("title").strip()
            toBeAppended["team1goals"] = int(goals[0].string.strip())
            toBeAppended["team2goals"] = int(goals[1].string.strip())
            toBeAppended["url"] = link.get("href")

            output.append(toBeAppended)
            print("completed " + link.get("href"))

    my_url.close()
    return output

def getCurrentSeason():
    engine = helper.getDatabaseEngine()
    Session = sessionmaker(bind=engine)
    session = Session()

    return session.query(Season.name).order_by(Season.startDate.desc()).first()[0]
