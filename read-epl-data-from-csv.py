import helper
import csv
from sqlalchemy.orm import sessionmaker
from models import Team, TeamAlias, Match

engine = helper.getDatabaseEngine()
Session = sessionmaker(bind=engine)
session = Session()

matches = []

with open('eplmatches.csv', 'r', encoding='utf-8-sig') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        matchDate = row[0]
        team1 = row[1]
        team2 = row[2]
        team1goals = int(row[3])
        team2goals = int(row[4])
        url = row[5]

        team1id = (session.query(Team).join(TeamAlias).filter(TeamAlias.alias == team1.upper()).first()).id
        team2id = (session.query(Team).join(TeamAlias).filter(TeamAlias.alias == team2.upper()).first()).id

        matches.append(Match(date=matchDate, team1id=team1id, team2id=team2id, team1goals=team1goals, team2goals=team2goals, url=url))

session.add_all(matches)
session.commit()
