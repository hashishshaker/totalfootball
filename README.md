Steps for running this repository:
1) Clone the repository on to your local machine.
2) Create a file called 'config.ini' that is a copy of 'config.sample.ini' and fill in the details of the local system.
3) python models.py
4) python seed.py